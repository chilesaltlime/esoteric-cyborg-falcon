import re


def detect_temperature(text):
    """detect temperature in text and return a list of temperatures and their units"""
    regex = re.compile(
        r"([+-]?(?:\d+(?:\.\d+)?))\s*(?:f(?:\b(?!\w)|\b)|c(?:\b(?!\w)|\b))",
        re.IGNORECASE,
    )
    matches = []
    for match in regex.finditer(text):
        temp = match.group(1)
        unit = match.group(0)[-1]
        matches.append((temp, unit.upper()))

    return "\n".join(
        [f"`{''.join(m)} is equal to {convert_to_c_or_f(m[0], m[1])}`" for m in matches]
    )


def convert_to_c_or_f(temp, unit):
    """convert temperature"""
    temp = float(temp)
    if unit == "F":
        return f"{round((temp - 32) * 5 / 9, 1)}C"
    elif unit == "C":
        return f"{round(temp * 9 / 5 + 32,1)}F"
