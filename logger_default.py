import logging
import pytz
from datetime import datetime


class custom_logger:
    """'a little logger that includes useful info like function and line no
    in addition to the regular stuff"""

    def __init__(self, name) -> None:
        self.name = name
        # create logger
        self.logger = logging.getLogger(self.name)
        self.logger.setLevel(logging.DEBUG)

        # create console handler and set level to info
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)

        # create formatter
        formatter = logging.Formatter(
            "%(asctime)s PT - %(levelname)s - %(name)s - %(funcName)s - %(lineno)d - %(message)s",
            "%Y-%m-%d %H:%M:%S",
        )

        # set timezone to pacific
        pacific_tz = pytz.timezone("US/Pacific")
        formatter.converter = lambda *args: datetime.now(pacific_tz).timetuple()
        # add formatter to ch
        ch.setFormatter(formatter)

        # add ch to logger
        self.logger.addHandler(ch)

        # append log outputs including errors to a file "logs.txt"
        fh = logging.FileHandler("logs.txt")
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)


if __name__ == "__main__":
    logger = custom_logger(__name__)
# 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
