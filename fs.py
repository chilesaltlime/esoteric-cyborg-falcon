from google.cloud import firestore


import os

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "service-acct.json"

db = firestore.Client()


def add_id(id):
    doc_ref = db.collection("ids").document("ids")
    doc_ref.set({"ids": firestore.ArrayUnion([id])}, merge=True)


def get_ids():
    doc_ref = db.collection("ids").document("ids")
    doc = doc_ref.get()
    return doc.to_dict()["ids"]
