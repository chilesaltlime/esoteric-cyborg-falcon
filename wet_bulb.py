from math import atan


def wet_bulb_tw(t, rh, units="f"):
    if units.lower() == "f":
        t = f_to_c(t)
    elif units.lower() != "c":
        return 0, "invalid unit, must be c or f"

    a = t * atan(0.151977 * (rh + 8.313659) ** 0.5)
    b = atan(t + rh) - atan(rh - 1.676331)
    c = 0.00391838 * rh ** (1.5) * atan(0.023101 * rh)
    d = -4.686035
    tw = a + b + c + d
    interpretation = interpet_tw(tw)
    if units == "f":
        tw = c_to_f(tw)
    return round(tw, 2), interpretation


def interpet_tw(tw):
    ftw = c_to_f(tw)

    if ftw > 90:
        return "Extreme Danger"
    if ftw > 88:
        return "High Danger"
    if ftw > 85:
        return "Moderate Danger"
    if ftw > 80:
        return "Low Danger"
    if ftw < 75:
        return "No Risk"


def f_to_c(f):
    return round((f - 32) * 5 / 9, 2)


def c_to_f(c):
    return round((c * 9 / 5) + 32, 2)


def get_wbt(t, rh, units="f"):
    t = float(t)
    rh = float(rh)
    return wet_bulb_tw(t, rh, units=units)
