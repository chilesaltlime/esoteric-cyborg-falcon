import pprint
import openai
import os
from logger_default import custom_logger
from time import sleep


def read_file_to_env():
    with open("openai_key.txt", "r") as f:
        lines = f.readlines()
        for line in lines:
            key, value = line.split("=")
            os.environ[key] = value.strip()


def pretty(s):
    return pprint.pformat(s)


async def reply_in_chat(prompt: list, user: int):
    logger.info(pretty(prompt[-1]))

    _temp = 0.9

    async def api_with_retry(prompt, user):
        for i in range(3):
            try:
                return await openai.ChatCompletion.acreate(
                    model="gpt-4",
                    messages=prompt,
                    user=str(user),
                    temperature=_temp,
                )

            except openai.error.RateLimitError as e:
                logger.warning(e)
                sleep(3)

    _response = await api_with_retry(prompt, user)
    _response = _response["choices"][0]["message"]["content"]
    logger.info(pretty(_response))
    logger.debug(f"Temperature: {_temp}")

    return _response[:1900]


def get_temp():
    import random

    mu = 1.1
    sigma = 0.25
    temp = round(random.gauss(mu, sigma), 2)

    temp = max(0.1, temp)
    temp = min(2, temp)
    return temp


read_file_to_env()
openai.organization = os.environ.get("OPENAI_ORG_KEY")
openai.api_key = os.environ.get("OPENAI_API_KEY")
logger = custom_logger(__name__).logger


# curl https://api.openai.com/v1/models -H "Authorization: Bearer $OPENAI_API_KEY" >> models.json
